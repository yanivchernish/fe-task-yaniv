(function () {
    "use strict";
    angular
        .module('app')
        .factory('wannabeHttpFactory', function ($http) {

            function getDummyData() {
                return $http.get('data/data.json')
            }

            return {
                getDummyData: getDummyData
            }

        });

})();