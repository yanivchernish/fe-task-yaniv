(function() {
    "use strict";

    angular
        .module('app')
        .controller('resultsController', function($scope, $filter, $state, $stateParams, $mdSidenav, $mdMedia ,wannabeHttpFactory) {
            var _this = this;
            _this.title = "Results found:";
            _this.results = null;
            _this.flights = null;
            _this.queryParams = {
                from: null,
                to: null,
                tickets: null
            };

            $scope.range = {
                lower: 10,
                upper: 2000
            };

            $scope.isDesktop = true;

            $scope.$on('$viewContentLoaded', function(e){

                _this.queryParams.from = $stateParams.from.toUpperCase();
                _this.queryParams.to = $stateParams.to.toUpperCase();
                _this.queryParams.tickets = parseInt($stateParams.tickets);

                console.log(_this.queryParams);

                wannabeHttpFactory.getDummyData().then(function(res){
                    _this.results = res.data.results;
                    _this.flights = res.data.flights;
                });
            });
        });
})();