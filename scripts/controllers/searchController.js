(function() {
    "use strict";

    angular
        .module('app')
        .controller('searchController', function($state) {
            var _this = this;
                _this.title = "Find your next vacation";
                _this.searchParams = {
                from: "",
                to: "",
                tickets: ''
                };

            _this.goToResults = function(){
                $state.go('results', {
                    from: _this.searchParams.from,
                    to: _this.searchParams.to,
                    tickets: _this.searchParams.tickets
                });
            }
        });
})();