(function () {

    "use strict";

    angular
        .module('app', ['ngMaterial', 'mdRangeSlider', 'ui.router', 'ui.router.router'])
        .config(function ($mdThemingProvider, $stateProvider, $urlRouterProvider) {

            $mdThemingProvider.theme('default')
                .primaryPalette('deep-orange')
                .accentPalette('blue');

            $stateProvider
                .state('search', {
                    url: '/search',
                    templateUrl: '/templates/search.html',
                    controller: 'searchController as controller'
                })
                .state('results', {
                    url: '/results/:from/:to/:tickets',
                    templateUrl: '/templates/results.html',
                    controller: 'resultsController as controller',
                    params: {
                        from: "TLV",
                        to: "BCN",
                        tickets: null
                    }
                });

            $urlRouterProvider.otherwise('/search');
        })
        .filter('rangeFilter', function(){
            return function (items, range){
                var filtered = [];
                var min = parseInt(range.lower);
                var max = parseInt(range.upper);
                angular.forEach(items, function(item) {
                    if( item.normalPrice >= min && item.normalPrice <= max ) {
                        filtered.push(item);
                    }
                });
                return filtered;
            }
        })
        .filter('destination', function(){
            return function (items, destination){
                var filtered = [];
                var to = destination.toUpperCase();
                angular.forEach(items, function(item) {
                    if( item.destination.toUpperCase() == to ) {
                        filtered.push(item);
                    }
                });
                return filtered;
            }
        })
})();

